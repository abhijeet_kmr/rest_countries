import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Content from "./components/Content";
import Header from "./components/Header";
import CountryDetails from "./components/CountryDetails";

import React, { Component } from "react";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Content} />
          <Route path="/countries/:code" component={CountryDetails} />
        </Switch>
      </Router>
    );
  }
}
