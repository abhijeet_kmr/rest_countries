import ClipLoader from "react-spinners/CircleLoader";
import * as CountryAPI from "./apiCall";
import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      isLoading: true,
      isError: false,
      selectedValue: "All",
      searchKeyWord: "",
      region: "All",
    };
  }

  componentDidMount() {
    CountryAPI.getCountries()
      .then((countries) =>
        this.setState({
          countries,
          isLoading: false,
        })
      )
      .catch((err) => {
        this.setState({
          isLoading: false,
          isError: "Server Did Not Respond!",
        });
      });
  }

  handleSelectOptionChange = (e) => {
    this.setState({
      region: e.target.value,
    });
  };

  handleChange = (e) => {
    console.log("clicked");
    this.setState({
      searchKeyWord: e.target.value,
    });
  };

  render() {
    if (this.state.isError) {
      return (
        <div>
          <h1>{this.state.isError}</h1>
        </div>
      );
    }

    const region = [
      ...this.state.countries.reduce((acc, curr) => {
        acc.add(curr.region);
        return acc;
      }, new Set()),
    ];

    const filtererdDataBySearch =
      this.state.searchKeyWord === ""
        ? this.state.countries
        : this.state.countries.filter((el) =>
            el.name.common
              .toLowerCase()
              .includes(this.state.searchKeyWord.toLocaleLowerCase())
          );

    const filtererdDataByRegion =
      this.state.region === "All"
        ? filtererdDataBySearch
        : filtererdDataBySearch.filter((el) => el.region === this.state.region);

    if (this.state.isLoading) {
      return (
        <div className="loader">
          <ClipLoader size={30} color={"#123abc"} loading={true} />
        </div>
      );
    }
    return (
      <>
        <div className="search-bar">
          <input
            type="text"
            placeholder="Search for a country..."
            onChange={this.handleChange}
            value={this.searchKeyWord}
          />

          <select onChange={this.handleSelectOptionChange}>
            <option value="All">Select by region</option>
            {region.map((value, i) => (
              <option key={i} value={value}>
                {value}
              </option>
            ))}
          </select>
        </div>

        <div className="main">
          <div className="content">
            {filtererdDataByRegion.map((value) => {
              return (
                <div key={value.cca2} className="country-cards">
                  <div className="img">
                    <img src={value.flags.png} alt={value.countryName} />
                  </div>
                  <div className="details">
                    <Link to={`/countries/${value.cca2}`}>
                      <h3>{value.name.common}</h3>
                    </Link>
                    <h4>
                      Population: <span>{value.population}</span>
                    </h4>
                    <h4>
                      Region: <span>{value.region}</span>
                    </h4>
                    <h4>
                      Capital: <span>{value.capital}</span>
                    </h4>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}
