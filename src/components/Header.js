import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <p>Where in the world?</p>
        <p>Dark Mode</p>
      </div>
    );
  }
}
