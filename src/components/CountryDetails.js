import ClipLoader from "react-spinners/CircleLoader";
import { Link } from "react-router-dom";

import React, { Component } from "react";
import * as CountryAPI from "./apiCall";
export default class CountryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: null,
      isLoading: true,
      isError: false,
    };
  }

  async componentDidMount() {
    try {
      const { code } = this.props.match.params;
      // console.log(code);
      const data = await CountryAPI.getCountryByCode(code);
      // console.log(country[0]);
      this.setState({
        country: data[0],
        isLoading: false,
        isError: false,
      });
    } catch (err) {
      this.setState({
        country: null,
        isLoading: false,
        isError: `failed to load by code`,
      });
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.match.params.code !== this.props.match.params.code) {
      try {
        const { code } = this.props.match.params;
        // console.log(code);
        const data = await CountryAPI.getCountryByCode(code);
        // console.log(country[0]);
        this.setState({
          country: data[0],
          isLoading: false,
          isError: false,
        });
      } catch (err) {
        this.setState({
          country: null,
          isLoading: false,
          isError: `failed to load by code`,
        });
      }
    }
  }

  render() {
    console.log(this.state.country);
    // console.log(code);

    if (this.state.isError) {
      return (
        <div>
          <h1>{this.state.isError}</h1>
        </div>
      );
    }
    if (this.state.isLoading) {
      return (
        <div className="loader">
          <ClipLoader size={30} color={"#123abc"} loading={true} />
        </div>
      );
    }

    return (
      <>
        <div>
          <Link to="/">
            <button className="back-button">Back</button>
          </Link>
        </div>
        <div className="details-container">
          <div className="flag">
            <img src={this.state.country.flags.png} alt="country flag" />
          </div>
          <div className="country-details">
            <h2>{this.state.country.name.common}</h2>
            <div className="sub-details">
              <div>
                <p>
                  Native Name:{" "}
                  <span className="span">{this.state.country.name.common}</span>
                </p>
                <p>
                  Population:{" "}
                  <span className="span">{this.state.country.population}</span>
                </p>
                <p>
                  Region:{" "}
                  <span className="span">{this.state.country.region}</span>
                </p>
                <p>
                  Sub Region:{" "}
                  <span className="span">{this.state.country.subregion}</span>
                </p>
                <p>
                  Capital:{" "}
                  <span className="span">{this.state.country.capital}</span>
                </p>
              </div>
              <div>
                <p>
                  Top Level Domain:{" "}
                  <span className="span">{this.state.country.tld}</span>
                </p>
                <p>
                  Currencies:{" "}
                  {Object.values(this.state.country.currencies).map((el, i) => {
                    return (
                      <span key={i} className="span">
                        {el.name}
                      </span>
                    );
                  })}
                </p>

                <p>
                  Languages:{" "}
                  {Object.keys(this.state.country.languages).map((el) => {
                    return (
                      <span key={el} className="span">
                        {this.state.country.languages[el]}&nbsp;
                      </span>
                    );
                  })}
                </p>
              </div>
            </div>
            {this.state.country.borders && this.state.country.borders.length ? (
              <p className="last-child">
                Border Countries:
                {this.state.country.borders?.map((border) => {
                  return (
                    <Link key={border} to={`/countries/${border}`}>
                      <button>{border}</button>
                    </Link>
                  );
                })}
              </p>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}
